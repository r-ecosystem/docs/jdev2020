
## []R, outils et usines logicielles[] - **Jean-François Rey** (INRAE)

JDEV2020 - le 09/07/2020 de 9h30 environ à 10h45.



Chat : [https://jdev-chat.insa-rennes.fr/channel/t4-usine\_logicielle\_et\_science\_reproductible](https://jdev-chat.insa-rennes.fr/channel/t4-usine\_logicielle\_et\_science\_reproductible)



### []Exemples Package R (code compilé) + App Shiny[]  :

   * Dépôt GitLab : [https://gitlab.paca.inrae.fr/CSIRO-INRA/landsepi](https://gitlab.paca.inrae.fr/CSIRO-INRA/landsepi)
   * App Shiny : [https://shiny.biosp.inrae.fr/app\_direct/landsepi/](https://shiny.biosp.inrae.fr/app\_direct/landsepi/)
   * Cookbooks : [https://gitlab.paca.inrae.fr/r-ecosystem](https://gitlab.paca.inrae.fr/r-ecosystem)


### []Idées de discussions : (en vrac)[]

    

   * Communauté R :
       *  R Foundation,
       *  RStudio,
       *  ThinkR
       * Blog
       * Communauté FR
   * Packages R :
       * devtools,
       * Renv,
       * Roxygen2,
       * ...
   * R Shiny :
       * Golem,
       * shinyapps.io,
       * shinyproxy,
       * ...
   * Dépôts :
       * GitHub,
       * GitLab,
       * ...
   * CI/CD :
       * Travis,
       * Jenkins,
       * R-hub,
       * GitLab CI/CD,
       * CRAN-like,
       * ...
   * Reproductibilité :
       * Docker,
       * MRAN,
       * Notebook
       * …


### []Questions : (Posez vos Questions ici)[]

   * Pourquoi GitLab ? (autohébergé, dépôt privé, secu, pas de solutions dispo en 2014) 
   * Quels investissement au départ ? Pas Pire. Courbe rapide 
   * Demo sur le pipeline App Shiny
   * Comment construit-on des tests pour une application shiny ?
       * Réponse de JFR: Il existe des possibilités de fichiers XML utilisables pour mimer les comportement des utilisateurs (Serenity sous java)
       * Il existe un package: "shinytest" qui permet de faire des tests d'intégration  (utilise PhantomJS)
   * Est-ce que l'INRAE fournit l'infrastructure pour faire tourner les images docker ? Kubernetes ? Openstack ?
   * Est-ce que les chercheurs de l'INRAE utilisent beaucoup Rcpp et est-ce facile pour eux ? Oui et Oui (modulo notre aide pour le C++)
   * Est-ce que l'utilisation de Python monte un peu ou R reste vraiment la première option ? Cela dépend des utilisations faites, les deux se valent
   * Utilisez-vous des GPU avec R chez vous ? Nop
   * Proposez-vous des solutions pour faire du big data comme des systèmes de fichiers distribués (HDFS) ou des cluster Spark mutualisés ? Non plus mais à étudier pour l'équipe OPE (PESV) de l'unité BioSP
   * quid de l'utilisation de Golem [https://github.com/ThinkR-open/golem](https://github.com/ThinkR-open/golem) ? Super outils si on ne veut pas se prendre la tête. JF Rey préfére ne pas l'utiliser et bidouiller soi-même.
   * 



### REX : (Envie de partager une expérience)

   * Github + travis, A tester GitHub Action (possibilité de partager des environnements), Travis peut être long
   * R-Hub (à ses débuts ~1 ans 1/2 mais prometteur pour faire des tests sur les packages R),
   * Singularity,
   * Solutions RStudio    

