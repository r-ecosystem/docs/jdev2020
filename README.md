# JDEV2020

[http://devlog.cnrs.fr/jdev2020](http://devlog.cnrs.fr/jdev2020)

Du 6 au 10 Juillet 2020 en distanciel.

Thématiques:
* [T4 : Usines logicielles et sciences reproductibles](http://devlog.cnrs.fr/jdev2020/t4)
* [T6 : DevOps dans l'ESR](http://devlog.cnrs.fr/jdev2020/t6)

## Poster
  * [GitLab CI/CD et environnement R](JF_REY-JDEV2020-GitLab_R.pdf)
  * [https://hal.archives-ouvertes.fr/hal-02899373](https://hal.archives-ouvertes.fr/hal-02899373 "hal-02899373")  


## REX

  * [GitLab CI/CD | Environnement R](GitLabCICD_R_JDEV2020.pdf)
  * [Slides avec commentaire](slide_JF_REY_GITLABCICD_R.mp4) (merci à Mayeul K. pour le mp4)  
  * [Presentation Video CanalU](https://www.canal-u.tv/video/jdev/jdev2020_t4_gitlab_ci_cd_pour_le_langage_r.57591) <--- Avec ma frimousse

## GT (Draft)
  * [R, outils et usines logicielles](http://devlog.cnrs.fr/jdev2020/t4.gt02)
    * [Support](T4GT02_slide.pdf)
    * [CR discussion](T4GT02_Note.md)

## Liens

  * [Exemples Package R](https://gitlab.paca.inrae.fr/r-ecosystem/cookbooks/r-packages-ci-cd)
  * [Exemples Apps R Shiny](https://gitlab.paca.inrae.fr/r-ecosystem/cookbooks/r-shiny-app-ci-cd)
    *  [Serveur public](https://shiny.biosp.inrae.fr/)
  * [+ Groups ecosystème R](https://gitlab.paca.inrae.fr/r-ecosystem/)
  * [Canal U pour toutes les présentations](https://www.canal-u.tv/producteurs/jdev/jdev2020)

## Références
 
  * [GitLab](https://about.gitlab.com/)
  * [GitLab-Runner](https://docs.gitlab.com/runner/)
  * [GitLab Registry](https://docs.gitlab.com/ee/user/packages/container_registry/)
  * [Docker](https://docker.com)
  * [VirtualBox](https://www.virtualbox.org/)
  * [R](https://www.r-project.org/)
  * [R Shiny](https://shiny.rstudio.com/)
  * [Shiny Server](https://rstudio.com/products/shiny/shiny-server/)
  * [ShinyProxy](https://www.shinyproxy.io/)
  * [Cookbook R](https://gitlab.paca.inrae.fr/r-ecosystem/cookbooks)

